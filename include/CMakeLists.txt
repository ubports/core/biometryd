add_subdirectory(biometry)

install(
  DIRECTORY biometry
  DESTINATION ${CMAKE_INSTALL_FULL_INCLUDEDIR}
  PATTERN "CMakeLists.txt" EXCLUDE
)
